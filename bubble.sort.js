var inputArray = [];
var size = 5; //Maximum Array size
function startBubbleSorting() {
  var input = alert(
    'This is Bubble sorting algorithm, You have to enter the elements of the array'
  );
  for (var i = 0; i < size; i++) {
    //Taking Input from user
    inputArray[i] = prompt('Enter Element ' + (i + 1));
  }
  //Print the array in the console.
  console.log(`input array is ${inputArray}`);
  var transformedArray = [...inputArray];
  console.log(`Sorted array is ${bubbleSort(transformedArray)}`);
  // console.log(bubbleSort(transformedArray));
  alert(
    'input array is' +
      JSON.stringify(inputArray) +
      'and sorted array is' +
      JSON.stringify(transformedArray)
  );
}

function swap(arr, firstIndex, secondIndex) {
  let temp = arr[firstIndex];
  arr[firstIndex] = arr[secondIndex];
  arr[secondIndex] = temp;
}

function bubbleSort(arr) {
  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < arr.length - 1; j++) {
      if (arr[j] > arr[j + 1]) {
        swap(arr, j, j + 1);
      }
    }
  }
  return arr;
}
